<%@page import="mvc.java.Model.UserDetails"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Details</title>
<script>  
var request=new XMLHttpRequest();  
function searchInfo(){  
var name=document.vinform.name.value;  
var url="search?val="+name;  
  
try{  
	//request.onreadystatechange=function(){  
	//if(request.readyState==4){  
	//var val=request.responseText;  
//	document.getElementById('mylocation').innerHTML=val;  
	//}  
	//}//end of function  
request.open("GET",url,true);  
request.send();  
}catch(e){alert("Unable to connect to server");}  
}  


function sortTable(n) {
	  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	  table = document.getElementById("myTable");
	  switching = true;
	  //Set the sorting direction to ascending:
	  dir = "asc"; 
	  /*Make a loop that will continue until
	  no switching has been done:*/
	  while (switching) {
	    //start by saying: no switching is done:
	    switching = false;
	    rows = table.rows;
	    /*Loop through all table rows (except the
	    first, which contains table headers):*/
	    for (i = 1; i < (rows.length - 1); i++) {
	      //start by saying there should be no switching:
	      shouldSwitch = false;
	      /*Get the two elements you want to compare,
	      one from current row and one from the next:*/
	      x = rows[i].getElementsByTagName("TD")[n];
	      y = rows[i + 1].getElementsByTagName("TD")[n];
	      /*check if the two rows should switch place,
	      based on the direction, asc or desc:*/
	      if (dir == "asc") {
	        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
	          //if so, mark as a switch and break the loop:
	          shouldSwitch= true;
	          break;
	        }
	      } else if (dir == "desc") {
	        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
	          //if so, mark as a switch and break the loop:
	          shouldSwitch = true;
	          break;
	        }
	      }
	    }
	    if (shouldSwitch) {
	      /*If a switch has been marked, make the switch
	      and mark that a switch has been done:*/
	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
	      switching = true;
	      //Each time a switch is done, increase this count by 1:
	      switchcount ++;      
	    } else {
	      /*If no switching has been done AND the direction is "asc",
	      set the direction to "desc" and run the while loop again.*/
	      if (switchcount == 0 && dir == "asc") {
	        dir = "desc";
	        switching = true;
	      }
	    }
	  }
	}
</script>  


</head>
<body>  
<h1>Search Employee</h1>  
<form name="vinform">  
<input type="text" name="name" onkeyup="searchInfo()">  
</form> 

<a href="new">Add UserDetails</a>

<% List<UserDetails> UserDetails=(List<UserDetails>)request.getAttribute("UserDetails"); %>
<table border='1' cellpadding='2' width='100%' id="myTable">
        <thead>
          <tr>
            <th><a href="short?short_item=Id">Id</a></th>
            <th><a href="short?short_item=Name">Name</a></th>
            <th><a href="short?short_item=Email_id">Email_Id</a></th>
            <th><a href="short?short_item=Phone_no">Phone_No</a></th> 
          </tr>
       </thead>   
        
		<tbody >
		  <% for(UserDetails userDetail : UserDetails) {%>
		  <% System.out.println(userDetail.getName());%>
          <tr>
            <td><%= userDetail.getId() %></td>
            <td><%= userDetail.getName() %></td>
            <td><%= userDetail.getEmail_id() %></td>
            <td><%= userDetail.getPhone_no() %></td>
            <td><a href="edit?id=<%= userDetail.getId()%>">Edit</a></td>
            <td><a href="delete?id=<%= userDetail.getId()%>">Delete</a></td>
          </tr>
          <% }%>
  </tbody>

</body>
</html>