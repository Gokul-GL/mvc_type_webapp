package mvc.java.Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.java.Model.UserDetailsServices;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	private UserDetailsServices userdetailsServices;
	
    public SearchServlet() {
        //super();
        // TODO Auto-generated constructor stub
    	this.userdetailsServices=new UserDetailsServices();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		sendRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		sendRequest(request, response);
		
		//doGet(request, response);
	}

	
	private void sendRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException
	{
		request.setAttribute("UserDetails", this.userdetailsServices.searchDetails(request.getParameter("val")));
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/View/UserDetails.jsp");
		dispatcher.forward(request, response);
	}
}
