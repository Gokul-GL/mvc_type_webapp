package mvc.java.Controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.java.Model.UserDetails;
import mvc.java.Model.UserDetailsServices;

/**
 * Servlet implementation class UserDetailsServlet
 */
@WebServlet("/")
public class UserDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
	
	private UserDetailsServices userdetailsServices;
	
    public UserDetailsServlet() {
        // TODO Auto-generated constructor stub
    	
    	this.userdetailsServices=new UserDetailsServices();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//sendRequest(request, response);
		
		String action = request.getServletPath();
		System.out.println(action);

        try {
            switch (action) {
            	case "/new":
            		newUserdetails(request, response);
            		break;
            	case "/insert":
            		insertUserDetails(request, response);
            		break;
                case "/delete":
                	System.out.println("Delete user");
                    deleteUser(request, response);
                    break;
                case "/edit":
                	request.setAttribute("UserDetails", this.userdetailsServices.getUserDetailsbyid(request.getParameter("id")));
                	RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/editUserDetails.jsp");
            		dispatcher.forward(request, response);
                   // editUserDetails(request, response);
                    break;
                case "/update":
                	editUserDetails(request, response);
                	break;    
                case "/short":
                	shortingData(request, response);
                	break;
                case "/search":
                	request.setAttribute("UserDetails", this.userdetailsServices.searchDetails(request.getParameter("val")));
            		RequestDispatcher dis=request.getRequestDispatcher("/WEB-INF/UserDetails.jsp");
            		dis.forward(request, response);
                	break;	
                default:
                    sendRequest(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		//adduserDetails(request, response);
		 
	}
	
	private void newUserdetails(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		System.out.println("new User");
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/addUserDetails.jsp");
		dispatcher.forward(request, response);
	}
	
	private void deleteUser(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException, SQLException{
		this.userdetailsServices.deleteUser(request.getParameter("id"));
        sendRequest(request, response);
	}
	
	private void sendRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException
	{
		request.setAttribute("UserDetails", this.userdetailsServices.getUserDetails());
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/UserDetails.jsp");
		dispatcher.forward(request, response);
	}
	
	private void insertUserDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		UserDetails user=new UserDetails();
		user.setName(request.getParameter("name"));
		user.setEmail_id(request.getParameter("email_id"));
		user.setPhone_no(request.getParameter("phone_no"));
		this.userdetailsServices.adduserDetails(user);
		sendRequest(request, response);
	}
	
	private void editUserDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		UserDetails user=new UserDetails();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setEmail_id(request.getParameter("email_id"));
		user.setPhone_no(request.getParameter("phone_no"));
		this.userdetailsServices.edituserDetails(user);
		sendRequest(request, response);
	}
//	private void updateDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
//		
//	}
	private void shortingData(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException
	{
		request.setAttribute("UserDetails", this.userdetailsServices.shortDetails(request.getParameter("short_item")));
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/UserDetails.jsp");
		dispatcher.forward(request, response);
	}

}
