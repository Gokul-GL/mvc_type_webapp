package mvc.java.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import ViewDetails.Userdetail;

public class UserDetailsServices {
	
	public static Connection getConnection(){  
        Connection con=null;  
        try{  
            Class.forName("com.mysql.jdbc.Driver");  
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/user_details","root","Admin@1234");  
        }catch(Exception e){System.out.println(e);}  
        return con;  
    }
	
	public void adduserDetails(UserDetails user)
	{
		Connection con=getConnection();
		try {
			PreparedStatement pre=con.prepareStatement("insert into user_details(Name,Email_id,Phone_no) value('"+user.getName()+"','"+user.getEmail_id()+"','"+user.getPhone_no()+"')");
			boolean rs=pre.execute();
			if(rs){
				System.out.println("%%%%%%% Data Inserted %%%%%%");
			}
			else{
				System.out.println("%%%%%%% Data Not Inserted %%%%%%");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<UserDetails> getUserDetails()
	{
		List<UserDetails> list=new ArrayList<UserDetails>();  
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select * from user_details");  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                UserDetails e=new UserDetails();
                e.setId(rs.getInt(1));
                e.setName(rs.getString(2));
                e.setEmail_id(rs.getString(3));
                e.setPhone_no(rs.getString(4));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);} 
		
		return list;
		
	}
	
	public List<UserDetails> getUserDetailsbyid(String id)
	{
		List<UserDetails> list=new ArrayList<UserDetails>();  
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select * from user_details where Id="+id);  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                UserDetails e=new UserDetails();
                e.setId(rs.getInt(1));
                e.setName(rs.getString(2));
                e.setEmail_id(rs.getString(3));
                e.setPhone_no(rs.getString(4));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);} 
		
		return list;
		
	}
	
	public List<UserDetails> searchDetails(String name)
	{
		List<UserDetails> list=new ArrayList<UserDetails>();  
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select * from user_details where Name like '"+name+"%'");  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                UserDetails e=new UserDetails();
                e.setId(rs.getInt(1));
                e.setName(rs.getString(2));
                e.setEmail_id(rs.getString(3));
                e.setPhone_no(rs.getString(4));
                System.out.println(rs.getString(2));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);} 
		
		return list;
		
	}
	
	
	public List<UserDetails> shortDetails(String short_item)
	{
		List<UserDetails> list=new ArrayList<UserDetails>();  
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select * from user_details ORDER BY "+short_item);  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                UserDetails e=new UserDetails();
                e.setId(rs.getInt(1));
                e.setName(rs.getString(2));
                e.setEmail_id(rs.getString(3));
                e.setPhone_no(rs.getString(4));
                System.out.println(rs.getString(2));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);} 
		
		return list;
		
	}

	public void edituserDetails(UserDetails user) {
		// TODO Auto-generated method stub
		Connection con=getConnection();
		try {
			PreparedStatement pre=con.prepareStatement("update user_details set Name='"+user.getName()+"',Email_id='"+user.getEmail_id()+"',Phone_no='"+user.getPhone_no()+"' WHERE id='"+user.getId()+"'");
			int rs=pre.executeUpdate();
			System.out.println("%%%%%%% Data Inserted %%%%%%");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteUser(String value) throws SQLException
	{
		Connection con=getConnection();
		int id=Integer.parseInt(value);
		System.out.println("DELETE USER");
		PreparedStatement ps;
		try {
			ps = con.prepareStatement(" DELETE FROM user_details WHERE Id="+id);
			 int rs=ps.executeUpdate(); 
			 System.out.println(rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
        
	}

}
